Nutch Whitelist Indexer Filter
======================

Very simple implementation of a filter. It prevents any url that doesn't have at least one whitelisted
word from being indexed by solr.

Compiling
==========

Maven and Java 7 should be enough to build this jar file, the process is extremely simple just run:

mvn clean intall

Installation
============

This uses the default nutch extension mechanism, installation can be completed by:

* Create a plugins/whitelist-filter folder inside nutch
* Copy the ${BUILD}/target/target/whitelist-filter-1.0-SNAPSHOT.jar and ${BUILD}/plugin.xml files to ${NUTCH}/plugins/whitelist-folder
* Create a conf/whitelist-indexfilter.txt file with your whitelist, just use a line for each word
* Edit the conf/nutch-default.xml file, adding the value whitelist-filter to the plugin.includes property
* Also add the following code to the nutch-default.xml file

    <property>
      <name>indexfilter.whitelist.file</name>
      <value>whitelist-indexfilter.txt</value>
      <description>Name of file on CLASSPATH containing whitelist words that indexed
      documents MUST contain.</description>
    </property>

Usage
======

Whenever you run any indexing command all of your indexing filters will run and prevent from indexing anything you don't want!

